import { Container } from "react-bootstrap"
import { Routes, Route } from "react-router-dom"
import { Home } from "./pages/Home"
import { Store } from "./pages/Store"
import { Montana } from "./pages/Montana"
import { Navbar } from "./components/Navbar"
import { ShoppingCartProvider } from "./context/ShoppingCartContext"
import { Gravel } from "./pages/Gravel"
import { CartProduct } from "./pages/CartProduct"
import Footer from "./components/Footer"
import Page404 from "./pages/Page404"
import TermsAndConditions from "./pages/TermsAndConditions"
import PrivacyPolicy from "./pages/PrivacyPolicy"


function App() {
  return (
    <ShoppingCartProvider>
      <Navbar></Navbar>
      <Container className="mb-4">
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/store" element={<Store />} />
          <Route path="/montana" element={<Montana />} />
          <Route path="/gravel" element={<Gravel></Gravel>} />
          <Route path="/cart-product" element={<CartProduct></CartProduct>} />
          <Route path="/terms-and-conditions" element={<TermsAndConditions></TermsAndConditions>} />
          <Route path="/privacy-polici" element={<PrivacyPolicy></PrivacyPolicy>} />
          <Route path="/*" element={<Page404></Page404>} />
        </Routes>
      </Container>
      <Footer></Footer>
    </ShoppingCartProvider>
  )
}

export default App
