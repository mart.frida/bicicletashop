import React from 'react';
import {
  MDBFooter,
  MDBContainer,
  MDBCol,
  MDBRow,
  MDBIcon,
  MDBBtn,
  MDBInput
} from 'mdb-react-ui-kit';

import 'mdb-react-ui-kit/dist/css/mdb.min.css';
import "@fortawesome/fontawesome-free/css/all.min.css";
import { Logo } from './logo/logo';

export default function Footer() {
  return (
    <MDBFooter className='shadow-lg bg-white text-center text-lg-start text-muted'>
      <section className='d-flex justify-content-center justify-content-lg-between p-4 border-bottom'>
        <div className='me-5 d-none d-lg-block'>
        </div>
        <div>
          <a href='' className='me-4 text-reset'>
            <MDBIcon color='secondary' fab icon='facebook-f' />
          </a>
          <a href='' className='me-4 text-reset'>
            <MDBIcon color='secondary' fab icon='twitter' />
          </a>
          <a href='' className='me-4 text-reset'>
            <MDBIcon color='secondary' fab icon='google' />
          </a>
          <a href='' className='me-4 text-reset'>
            <MDBIcon color='secondary' fab icon='instagram' />
          </a>
          <a href='' className='me-4 text-reset'>
            <MDBIcon color='secondary' fab icon='linkedin' />
          </a>
          <a href='' className='me-4 text-reset'>
            <MDBIcon color='secondary' fab icon='github' />
          </a>
        </div>
      </section>

      <section className=''>
        <MDBContainer className='text-center text-md-start mt-5'>
          <MDBRow className='mt-3'>
            <MDBCol md='3' lg='4' xl='3' className='mx-auto mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>
                <Logo></Logo>
              </h6>
              <p>
                Ofrecemos no solo los mejores precios, sino también un servicio de primera clase y un asesoramiento competente para nuestros clientes: eso es lo que nos distingue.
              </p>
            </MDBCol>

            <MDBCol md='2' lg='2' xl='2' className='mx-auto mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>Products</h6>
              <p>
                <a href='/montana' className='text-reset'>
                  Montana
                </a>
              </p>
              <p>
                <a href='/gravel' className='text-reset'>
                  Gravel
                </a>
              </p>
              <p>
                <a href='/gravel' className='text-reset'>
                  Carretera
                </a>
              </p>
              <p>
                <a href='/gravel' className='text-reset'>
                  Bmx
                </a>
              </p>
            </MDBCol>

            <MDBCol md='3' lg='2' xl='2' className='mx-auto mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>Useful links</h6>
              <p>
                <a href='#!' className='text-reset'>
                  Pricing
                </a>
              </p>
              <p>
                <a href='/terms-and-conditions' className='text-reset'>
                  Termos e Condições
                </a>
              </p>
              <p>
                <a href='/privacy-polici' className='text-reset'>
                  Privacy Polici
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  Help
                </a>
              </p>
            </MDBCol>

            <MDBCol md='4' lg='3' xl='3' className='mx-auto mb-md-0 mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>Contact</h6>
              <p>
                <MDBIcon color='secondary' icon='home' className='me-2' />
                Barcelona, BS 10012, ES
              </p>
              <p>
                <MDBIcon color='secondary' icon='envelope' className='me-3' />
                contato@bikcraft.com
              </p>
              <p>
                <MDBIcon color='secondary' icon='phone' className='me-3' /> + 34 234 567 88
              </p>
              <p>
                <MDBIcon color='secondary' />Monday - Friday 08:00 - 18:00 <br /> Saturdays 09:00-13:00
              </p>

            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </section>

      <div className='text-center p-4' style={{ backgroundColor: 'rgba(0, 0, 0, 0.05)' }}>
       <span> © 2023 Copyright: </span> 
        <a className='text-reset fw-bold' href='/'>
         Bicicleta.com
        </a>
      </div>
    </MDBFooter>
  );
}