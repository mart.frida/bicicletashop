import { ReactNode } from "react"
import { InputGroup, Form } from "react-bootstrap"

type FormWrapperProps = {
  title: string
  children: ReactNode
}

export function FormWrapper({ title, children }: FormWrapperProps) {
  return (
    <>
      <h2 style={{ textAlign: "center", margin: 0, marginBottom: "2rem" }}>
        {title}
      </h2>
      <InputGroup className="mb-3">
                 {children}
        
        {/* <Form.Control
          aria-label="Default"
          aria-describedby="inputGroup-sizing-default"
        /> */}
      </InputGroup>
      <br />
    </>
  )
}
