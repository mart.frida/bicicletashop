import { InputGroup, Form } from "react-bootstrap"
import { FormWrapper } from "./FormWrapper"

type AccountData = {
  tel: string
  email: string
  password: string
}

type AccountFormProps = AccountData & {
  updateFields: (fields: Partial<AccountData>) => void
}

export function AccountForm({
  tel,
  email,
  password,
  updateFields,
}: AccountFormProps) {
  return (
    <FormWrapper title="Contact Creation">
      <InputGroup className="mb-3">
        <InputGroup.Text>
          Tel
        </InputGroup.Text>
        <Form.Control
          autoFocus
          required
          type="tel"
          value={tel}
          onChange={e => updateFields({ tel: e.target.value })}
        />
      </InputGroup>
      <br />
      <InputGroup className="mb-3">
        <InputGroup.Text>
          Email
        </InputGroup.Text>
        <Form.Control
          autoFocus
          required
          type="email"
          value={email}
          onChange={e => updateFields({ email: e.target.value })}
        />
         {/* <p className="text-muted">We'll never share your email with anyone else.</p> */}
      </InputGroup>
      <br />
      <InputGroup className="mb-3">
        <InputGroup.Text>
          Password
        </InputGroup.Text>
        <Form.Control
          autoFocus
          required
          type="password"
          value={password}
          onChange={e => updateFields({ password: e.target.value })}
        />
      </InputGroup>
      <br />
    </FormWrapper>
  )
}
