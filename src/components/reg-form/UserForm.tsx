import { useState } from "react"
import { Button, Form, InputGroup, Modal } from "react-bootstrap"
import { FormWrapper } from "./FormWrapper"

type UserData = {
  firstName: string
  lastName: string
}

type UserFormProps = UserData & {
  updateFields: (fields: Partial<UserData>) => void
}

export function UserForm({
  firstName,
  lastName,
  updateFields,
}: UserFormProps) {
  return (
    <FormWrapper title="User Name" >
      <InputGroup className="mb-3">
        <InputGroup.Text>
          First Name
        </InputGroup.Text>
        <Form.Control
          autoFocus
          required
          value={firstName}
          onChange={e => updateFields({ firstName: e.target.value })}
        />
      </InputGroup>
      <br />
      <InputGroup className="mb-3">
        <InputGroup.Text >
          Last Name
        </InputGroup.Text>
        <Form.Control
          required
          value={lastName}
          onChange={e => updateFields({ lastName: e.target.value })}
        />
      </InputGroup>
    </FormWrapper>
  )
}
