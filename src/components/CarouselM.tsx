import Carousel from 'react-bootstrap/Carousel';

import dProducts from "../data/dproducts.json"

type ImgItemProps = {
  name: string,
  imgUrl: string
}

interface CarouselMProps{
  category:string
}

export function CarouselM({category}:CarouselMProps) {

  return (
    <Carousel variant="dark" pause='hover' interval={3000} style={{ width: "min-content", margin: "0 auto" }} >
      {dProducts.map(imgItem => 
      (category==imgItem.category)&&(
        <Carousel.Item >
          <img
            style={{ height: "80vh" }}
            className="d-block m-3"
            src={imgItem.imgUrl}
            alt={imgItem.name}
          />
        </Carousel.Item>
      ))}
    </Carousel>

  )
}
