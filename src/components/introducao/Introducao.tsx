import Image from 'react-bootstrap/Image'

import dProducts from "../../data/dproducts.json"
import "./introducao.css"

export default function Introducao() {
  return (
    <div className="introducao">
      <div className="content">
        <h2 >Bicicletas feitas sob medida. </h2>
        <p>
          Bicicletas de alta precisão e qualidade, feitas sob medida
          para o cliente. Explore o mundo na sua velocidade com a Bikcraft.
        </p>
        <a href="/montana"><button>SELECIONE A SUA</button> </a>

      </div>

      {(dProducts.map(el => (el.category == "intoducao") && <Image src={el.imgUrl} alt={el.name}></Image>))}

    </div>
  )
}