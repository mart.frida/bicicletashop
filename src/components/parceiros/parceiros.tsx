import "../../styles/iconsfont.css"
import "../parceiros/parcerious.css"

export default function Parceiros(){
  return(
    <section className="parceiros">
        <h2 className="font-1-xxl">nossos parceiros.</h2>
        <table>
            <tr>
                <td><i className="logo icon-caravan"></i> </td>
                <td><i className="logo icon-ranek"></i> </td>
                <td><i className="logo icon-handel"></i> </td>
                <td><i className="logo icon-dogs"></i> </td>
            </tr>
            <tr>
                <td><i className="logo icon-lescone"></i> </td>
                <td><i className="logo icon-flexblog"></i> </td>
                <td><i className="logo icon-wildbeast"></i> </td>
                <td><i className="logo icon-surfbot"></i> </td>
            </tr>
        </table>
    </section>
  )
}