import { Link } from "react-router-dom";

import "./logo.css";
import "../../styles/iconsfont.css";

export function Logo(){
  return (
    <div className="logo">
      <Link to="/" className={"icon-bikcraft"}></Link>
    </div>
  );
};