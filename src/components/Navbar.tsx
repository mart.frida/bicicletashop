import { Button, Container, Nav, Navbar as NavbarBs } from "react-bootstrap"
import { NavLink } from "react-router-dom"
import { useShoppingCart } from "../context/ShoppingCartContext"
import { Logo } from "./logo/logo"

export function Navbar() {
  const { openCart, cartQuantity } = useShoppingCart()
  return <NavbarBs sticky="top" className="bg-white shadow-sm mb-3">
    <Container>
      <Logo></Logo>
      <Nav className="right-aligned">
        <Nav.Link to='/' as={NavLink}>Home</Nav.Link>
        <Nav.Link to='/store' as={NavLink}>Store</Nav.Link>
        <Nav.Link to='/montana' as={NavLink}>Montana</Nav.Link>
        <Nav.Link to='/gravel' as={NavLink}>Gravel</Nav.Link>
      </Nav>
      {cartQuantity > 0 && (
        <Button
          onClick={openCart}
          style={{ width: "3rem", height: "3rem", position: "relative" }}
          variant="outline-primary" className="rounded-circle">
          <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-basket3" viewBox="0 0 16 16">
            <path d="M5.757 1.071a.5.5 0 0 1 .172.686L3.383 6h9.234L10.07 1.757a.5.5 0 1 1 .858-.514L13.783 6H15.5a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H.5a.5.5 0 0 1-.5-.5v-1A.5.5 0 0 1 .5 6h1.717L5.07 1.243a.5.5 0 0 1 .686-.172zM3.394 15l-1.48-6h-.97l1.525 6.426a.75.75 0 0 0 .729.574h9.606a.75.75 0 0 0 .73-.574L15.056 9h-.972l-1.479 6h-9.21z" />
          </svg>

          <div className="rounded-circle bg-danger d-flax justify-content-center align-items-center"
            style={{
              color: "white",
              width: "1.5rem",
              height: "1.5rem",
              position: "absolute",
              bottom: 0,
              right: 0,
              transform: "translate(25%, 25%)"
            }}
          >{cartQuantity}</div>
        </Button>
      )}
    </Container>
  </NavbarBs>
}
