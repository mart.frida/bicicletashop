import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import RegForm from './reg-form/RegForm';

export function StartOrder() {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <Button variant="primary" onClick={handleShow}>
        Quick Order
      </Button>

      <Modal show={show} onHide={handleClose}>
        <RegForm></RegForm>
      </Modal>
    </>
  );
}
