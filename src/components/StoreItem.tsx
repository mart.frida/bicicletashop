import { Button, Card } from "react-bootstrap"
import { useShoppingCart } from "../context/ShoppingCartContext"
import { formatCurrency } from "../utilites/formatCurrency"
import { useState } from "react"

type StoreItemProps = {
  id: number,
  category: string,
  name: string,
  size?: string,
  tamano?: number,
  color?: string,
  producer?: string,
  price: number,
  imgUrl: string
}

export function StoreItem({ id, name, price, imgUrl }: StoreItemProps) {
  const [isHover, setIsHover] = useState(false);
  const handleMouseEnter = () => {
    setIsHover(true);
  };
  const handleMouseLeave = () => {
    setIsHover(false);
  };


  const { getItemQuaintity,
    increaseCartQuantity,
    decreaseCartQuantity,
    removeFromCart,
  } = useShoppingCart()

  const quantity = getItemQuaintity(id)
  return (<Card className={isHover ? 'h-110 shadow' : 'h-100 shadow-sm'} onMouseEnter={handleMouseEnter}
  onMouseLeave={handleMouseLeave} >
    <Card.Img
      variant="top"
      src={imgUrl}
      height="250px"
      style={{ objectFit: "cover" }}>
    </Card.Img>
    <Card.Body className="d-flex flex-column">
      <Card.Title className="d-flex justify-content-between align-items-baseline mb-4">
        <span className="fs-2">{name}</span>
        <span className="ms-2 text-muted">{formatCurrency(price)}</span>
      </Card.Title>
      <div className="mt-auto">
        {/* <Button href="/cart-product" variant="secondary" className="w-100 mb-2">Take a closer look</Button> */}

        {quantity === 0 ? (
          <Button className="w-100" onClick={() => increaseCartQuantity(id)}>+ Add To Bag</Button>)
          : (<div
            className="d-flex align-items-center flex-column" style={{ gap: ".5rem" }}>
            <div
              className="d-flex align-items-center justify-content-center"
              style={{ gap: ".5rem" }}>
              <Button onClick={() => decreaseCartQuantity(id)}>-</Button>
              <div>
                <span className="fs-3">
                  {quantity}</span> in cart
              </div>
              <Button onClick={() => increaseCartQuantity(id)}>+</Button>
            </div>
            <Button variant="danger" size="sm" onClick={() => removeFromCart(id)}>Remove</Button>
          </div>)}
      </div>
    </Card.Body>
  </Card>)
}