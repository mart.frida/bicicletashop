import { Col, Row } from "react-bootstrap"
import { StoreItem } from "../components/StoreItem"
// import storeItems from "../data/items.json"
import storeItems from "../data/dproducts.json"

export function Store() {
  return (
  <div>
    <h1>Store</h1>
    <Row md={2} xs={1} lg={3} className="g-3" style={{transition: "all .3s cubic-bezier(0,0,.5,1)"}}>
      {storeItems.map(item => (
        <Col key={item.id}><StoreItem {...item} /></Col>
      ))}
    </Row>
  </div>
  )
}