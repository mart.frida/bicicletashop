import { Col, Row } from "react-bootstrap"
import { CarouselM } from "../components/CarouselM"
import { StoreItem } from "../components/StoreItem"

import storeItems from "../data/dproducts.json"


export function Montana() {
  return (
    <div>
      <h1>Montaña</h1>
      <Row md={2} xs={1} lg={3} className="g-3">
        {storeItems.filter(item =>  item.category=="montana" ).map(item => (
          <Col key={item.id}><StoreItem {...item} /></Col>
        ))
        }
      </Row>
      <CarouselM category="montana"></CarouselM>
    </div>
  )

}