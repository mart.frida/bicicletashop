export default function TermsAndConditions() {
  return (<div>
    <h1>
      Terms and Conditions
    </h1>
    <p>General terms and conditions <br />
      of the company Bicraft<br />
      as per the Spain Civil Code<br />
      valid as of: July 2022</p>
    <h3>Making a purchase</h3>
    <ol>
      <li>
        <p><em>Order</em> <br />
        We offer the possibility to order conveniently via our online-shop or by phone. For mutual safety, orders from abroad are not accepted by telephone.
      </p></li>
      <li>
        <p>
          <em>How to pay</em> <br />
          <ol type="a">
            <li>
              <p><em>By C.O.D. (cash on delivery)</em><br />
                The delivery is effected via DHL. You pay the amount to the carrier directly on receipt of the goods. Please acknowledge that in addition to our internal C.O.D. charge of 4,- € a postal surcharge of 2,- € will be charged upon delivery by the deliverer when choosing C.O.D. A delivery by C.O.D. is only possible within Spain.
              </p>
            </li>
            <li>
              <p><em>By payment in advance</em><br />
                When ordering by payment in advance, the ordered goods will be sent as soon as the invoice amount is credited to our bank account.
              </p>
            </li>
            <li>
              <p><em>By credit card / PayPal</em><br />
              For our customers, we offer payment by Mastercard or Visa credit cards.
              </p>
            </li>
            <li>
              <p><em>By voucher</em><br />
              Please note that the amount of the gift-voucher (if you have several gift-vouchers: the sum of all gift-vouchers) can not be higher than the order amount. Vouchers are not redeemable for cash.
              </p>
            </li>
          </ol>
        </p></li>
        <li>
        <p><em>Delivery</em> <br />
        We choose the <strong>most favourable way of dispatch</strong> for your delivery. Of course we deliver your orders at your home address. Should the carrier not be able to deliver the goods, we will inform you in writing. You then have the choice to either have the goods delivered again or to pick up the goods at a depositary near your home address. If not avoidable, we reserve the right of effecting partial deliveries.
      </p></li>
    </ol>
  </div>)
}