import { useEffect, useState } from "react";
import { Button } from "react-bootstrap";

export function CartProduct() {
  const [type, setType] = useState(" ")
  const [data, setData] = useState([])

  useEffect(() => {
    fetch(`https://jsonplaceholder.typicode.com/${type}/1`)
      .then((response) => response.json())
      .then((json) => setData(json))
  }, [type])


  return (
    <>
      <h1>Cart Product</h1>
      <Button onClick={() => setType("posts")}>More details</Button>
      <Button onClick={() => setType("photos")}>More photos</Button>
      <Button onClick={() => setType("comments")}>Comments</Button>
      <pre>{JSON.stringify(data, null, 2)}</pre>
    </>
  )
}