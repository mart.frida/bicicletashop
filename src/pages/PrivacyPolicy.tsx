export default function PrivacyPolicy() {
  return (<>
    <h1>Privacy Policy</h1>
    <ul>
      <li><h4>ACCESS DATA AND HOSTING</h4>
        <p>
          You may visit our website without revealing any personal information. With every visit on the website, the web server stores automatically only a so-called server log file which contains e.g. the name of the requested file, your IP address, the date and time of the request, the volume of data transferred and the requesting provider (access data), and documents the request. These access data are analysed exclusively for the purpose of ensuring the smooth operation of the website and improving our offer. This serves according to Art. 6 (1) 1 lit. f GDPR the protection of our legitimate interests in the proper presentation of our offer that are overriding in the process of balancing of interests. All access data are deleted no later than seven days after the end of your visit on our website.
        </p></li>
      <li><h4>DATA PROCESSING FOR THE PURPOSES OF PROCESSING THE CONTRACT, ESTABLISHING CONTACT</h4>
        <p>
          For the purpose of performing the contract (including enquiries regarding the processing of any existing warranty and performance fault claims as well as any statutory updating obligations) in accordance with Art. 6 (1) (b) GDPR, we collect personal data if you provide it to us voluntarily as part of your order. Mandatory fields are marked as such, as in these cases we necessarily need the data to process the contract and we cannot send the order without their specification. Which data is collected can be seen from the respective input forms.
        </p></li>
      <li><h4>DATA PROCESSING FOR THE PURPOSES OF PAYMENT</h4>
        <p>
          Depending on the selected payment method, we forward the data necessary for processing the payment transaction to our technical service providers, who act for us on the basis of processing on our behalf or to the authorised credit institutions or to the selected payment service provider insofar as this is necessary for the payment process. This serves the fulfilment of the contract according to Art. 6 (1) (b) GDPR. In certain cases, payment service providers collect the data required for processing the payment themselves, e.g. on their own website or via technical solution within the ordering process. In this respect, the privacy policy of the respective payment service provider applies. If you have any questions about our payment processing partners and the basis of our cooperation with them, please use the contact option described in this privacy policy.
        </p></li>
    </ul>

  </>)
}