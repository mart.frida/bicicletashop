import { CarouselM } from "../components/CarouselM";
import Introducao from "../components/introducao/Introducao";
import Parceiros from "../components/parceiros/parceiros";

export function Home() {
  return (
    <div>
      <Introducao></Introducao>
      {/* <Parceiros></Parceiros> */}
      <CarouselM category="city"></CarouselM>
    </div>
  )

}